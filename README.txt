
Module: ComScore
Author: Victor Galguera <http://drupal.org/user/1335082>


Description
===========
Adds the ComScore tracking system to your website.

Requirements
============

* ComScore customer account


Installation
============
* Copy the 'comscore' module directory in to your Drupal
sites/all/modules directory as usual.


Usage
=====
In the settings page enter your ComScore customer code and website code.

All pages will now have the value-added JavaScript code to the
HTML header can confirm seeing the source of the page
your browser.
